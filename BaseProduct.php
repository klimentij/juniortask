<?php
abstract class BaseProduct
{
    protected $name;
    protected $sku;
    protected $price;
    protected $type;

    abstract protected function connectDB();
    abstract public function save();
    abstract public function load($id);
    abstract public function delete($id);
    abstract public function getAll();
    public function setName($n) {
        $this->name = $n;
    }
    public function setSku($s) {
        $this->sku = $s;
    }
    public function setPrice($p) {
        $this->price = $p;
    }
}
?>