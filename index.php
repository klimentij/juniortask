<?php
/**
 * This test task was completed for Junior Developer Test by Kliments Krasovskis
 * All general requirements noted in test was implemented
 */
    include_once 'SizeProduct.php';
    include_once 'WeightProduct.php';
    include_once 'DimensionProduct.php';
    $sizeProducts = new SizeProduct();
    $weightProducts = new WeightProduct();
    $dimProducts = new DimensionProduct();
?>
<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src='js/jquery-3.4.1.min.js'></script>
    <script src='js/my.js'></script>
</head>
<body>
    <nav class="navbar navbar-light bg-light">
    <a class="navbar-brand">Product List</a>
    <div class="form-inline">
        <select id="act" class="form-control">
            <option value="add">Add Action</option>
            <option value="delete">Mass Delete Action</option>
        </select>
        <button onclick="redirectaction();" class="btn btn-outline-success my-sm-0" >Apply</button>
</div>
    </nav>
    <div class="container">
        <div class="row">
            <div class='row row-cols-4'>
                <?php echo $sizeProducts->getAll(); ?>
                <?php echo $weightProducts->getAll(); ?>
                <?php echo $dimProducts->getAll(); ?>
            </div>      
        </div>
    </div>
</body>
</html>