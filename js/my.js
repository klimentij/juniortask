$(document).ready(function () {
    $("#sizeform").show();
    $("#weightform").hide();
    $("#dimform").hide();
})
function changeform() {
    var type = $("#type").val();
    if(type == '1') {
        $("#sizeform").show();
        $("#weightform").hide();
        $("#dimform").hide();
    }
    else if(type == '2') {
        $("#sizeform").hide();
        $("#weightform").show();
        $("#dimform").hide();
    }
    else {
        $("#sizeform").hide();
        $("#weightform").hide();
        $("#dimform").show();
    }
}
function redirectaction() {
    if($("#act").val() == 'add') location.replace('/juniortask/new_product.html');
    if($("#act").val() == 'delete')
    {
        var i = 0;
        var ids = [];
        $(".cardnum").each(function() {
            if($(this).prop("checked")) {
                ids[i] = $(this).val();
                i++;
            }
        });
        console.log(ids);
        $.post("delete.php", { 'ids[]': ids }, function( data ) {
            console.log(data);
            location.replace('/juniortask/');
          });
    }
    
}