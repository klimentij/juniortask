<?php
    include_once 'SizeProduct.php';
    include_once 'WeightProduct.php';
    include_once 'DimensionProduct.php';
    $idArr = $_POST['ids'];
    $sizeProducts = new SizeProduct();
    $weightProducts = new WeightProduct();
    $dimProducts = new DimensionProduct();
    foreach ($idArr as $id) {
        if($sizeProducts->load($id)) {//check if ID from ids list matches to the size type product
            $sizeProducts->delete($id);//if yes, delete it
        }
        if($weightProducts->load($id)) {
            $weightProducts->delete($id);
        }
        if($dimProducts->load($id)) {
            $dimProducts->delete($id);
        }
    }
?>