<?php
    $user = "site";
    $pass = "1234";
    try {
        //make connection string and allow to show SQL errors
        $dbConnection = new PDO('mysql:host=localhost;dbname=dbShop', $user, $pass);
        $dbConnection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        
        $sql = "CREATE TABLE products (
                id INT AUTO_INCREMENT,
                sku VARCHAR(10) NOT NULL,
                name VARCHAR(255) NOT NULL,
                price DECIMAL NOT NULL,
                type INT NOT NULL,
                size INT,
                weight INT,
                height INT,
                width INT,
                lenght INT,
                PRIMARY KEY (id))";
        print "Info: Table products created<br/>";
        $dbConnection->exec($sql);
        $dbConnection = null;
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }    
?>