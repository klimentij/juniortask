<?php
include_once 'SizeProduct.php';
include_once 'WeightProduct.php';
include_once 'DimensionProduct.php';

if($_POST["type"] == 1)
{
    $newProduct = new SizeProduct();
    $newProduct->setName($_POST["name"]);
    $newProduct->setSku($_POST["sku"]);
    $newProduct->setPrice($_POST["price"]);
    $newProduct->setSize($_POST["size"]);
    $newProduct->save();
}
if($_POST["type"] == 2)
{
    $newProduct = new WeightProduct();
    $newProduct->setName($_POST["name"]);
    $newProduct->setSku($_POST["sku"]);
    $newProduct->setPrice($_POST["price"]);
    $newProduct->setWeight($_POST["weight"]);
    $newProduct->save();
}
if($_POST["type"] == 3)
{
    $newProduct = new DimensionProduct();
    $newProduct->setName($_POST["name"]);
    $newProduct->setSku($_POST["sku"]);
    $newProduct->setPrice($_POST["price"]);
    $newProduct->setWidth($_POST["width"]);
    $newProduct->setHeight($_POST["height"]);
    $newProduct->setLenght($_POST["lenght"]);
    $newProduct->save();
}
?>