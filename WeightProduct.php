<?php
include_once "BaseProduct.php";

class WeightProduct extends BaseProduct {
    public $weight;

    //create connection to DB
    protected function connectDB()
    {
        $connection = new PDO('mysql:host=localhost;dbname=dbShop', 'site', '1234');
        $connection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        return $connection;
    }
    public function setWeight($w)
    {
        $this->weight = $w;
    }
    public function save()
    {
        $db = $this->connectDB();
        $this->type = 2;
        $sql = "INSERT INTO products (sku, name, price, type, weight) VALUES (?, ?, ?, ?, ?)";
        $res = $db->prepare($sql)->execute([$this->sku, $this->name, $this->price, $this->type, $this->weight]);
        header('Location: /juniortask/');
    }
    public function load($id)
    {
        $db = $this->connectDB();
        $sql = "select * from products where type = 2 and id = " . $id;
        $row = $db->prepare($sql);
        if($row->execute()) {
            $res = $row->fetch();
            $this->sku = $res['sku'];
            $this->name = $res['name'];
            $this->price = $res['price'];
            $this->type = $res['type'];
            $this->weight = $res['weight'];
            return true;
        }
        return false;
    }
    public function delete($id)
    {
        $db = $this->connectDB();
        $sql = "delete from products where id = " . $id;
        $row = $db->prepare($sql);
        $row->execute();
        header('Location: /juniortask/');
    }
    //load all data matching current object type
    //and generate html code
    public function getAll()
    {
        $db = $this->connectDB();
        $sql = "select * from products where type = 2";
        $result = null;

        foreach($db->query($sql) as $row) {
            $result = $result . "
            <div class='col mb-2'>
                <div class='card' style='width: 18rem;'>
                    <div class='card-body'>
                        <input type='checkbox' class='cardnum' value=" . $row['id'] . "> 
                        <p class='card-text text-center'>" . $row['sku'] . "</p>
                        <h5 class='card-title text-center'>" . $row['name'] . "</h5>
                        <p class='card-text text-center'>Price: " . $row['price'] . "&#36;</p>
                        <p class='card-text text-center'>Weight: " . $row['weight'] . "</p>
                    </div>
                </div>
            </div>";
        }
        return $result;
    }
}
?>